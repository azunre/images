FROM ubuntu:bionic

# nvidia-docker 1.0
LABEL com.nvidia.volumes.needed="nvidia_driver"
LABEL com.nvidia.cuda.version="${CUDA_VERSION}"

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=9.0"

ENV DEBIAN_FRONTEND noninteractive

# install nvidia driver
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:graphics-drivers/ppa
RUN apt update
# For nvidia-docker, no need to install GPU driver?
# if installed, can use without nvidia-docker, which will require host driver to match exactly
RUN apt install -y nvidia-384 -y nvidia-384-dev
RUN apt-get install -y g++ freeglut3-dev build-essential libx11-dev libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev
RUN apt install -y gcc-6
RUN apt install -y g++-6

# install cuda 9.0
RUN apt install -y wget
RUN wget https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run
RUN chmod +x cuda_9.0.176_384.81_linux-run 
RUN ./cuda_9.0.176_384.81_linux-run --override --silent
RUN mkdir -p /usr/local/cuda/bin
RUN ln -s /usr/bin/gcc-6 /usr/local/cuda/bin/gcc
RUN ln -s /usr/bin/g++-6 /usr/local/cuda/bin/g++

# install cudnn 7
COPY required_files/libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb /tmp/libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb
RUN chmod +x /tmp/libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb
RUN dpkg -i /tmp/libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Prevent Python packages to be installed through APT.
COPY ./etc/apt/preferences.d/no-python2 /etc/apt/preferences.d/no-python2

# General dependencies for building dependencies, locales, and some utilities.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends swig git build-essential cmake wget ssh tar gzip ca-certificates unzip curl libcurl4-openssl-dev libssl-dev equivs vim-tiny && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 update-alternatives --install /usr/bin/vim vim /usr/bin/vim.tiny 10 && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Set locale to UTF-8 which makes Python read text in UTF-8 and not ASCII, but default.
ENV LC_ALL=en_US.UTF-8

# Installing Python 3.6.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes python3.6 python3.6-dev python3-pip python3-openssl && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Base Python packages (those dependencies from d3m core package which are needed also in testing image).
RUN pip3 install docker[tls]==2.7 && \
 pip3 install --upgrade setuptools && \
 pip3 install frozendict==1.2 && \
 pip3 install PyYAML==3.12 && \
 pip3 install pycurl==7.43.0.1 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dependencies needed to build Python itself.
RUN apt-get update -q -q && \
 apt-get build-dep --yes --force-yes python3.6 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Extra Python dependencies.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends zlib1g-dev libexpat1-dev tk8.6-dev libffi-dev libssl-dev \
 libbz2-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libgdbm-dev liblzma-dev lzma lzma-dev && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Dummy package to satisfy unnecessary dependency on Python 2.
# See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=891712
COPY ./equivs/python-dev /tmp/python-dev
RUN cd /tmp && \
 equivs-build python-dev && \
 dpkg -i python-dev-dummy_1.0_all.deb && \
 cd / && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

## ADDITIONAL GPU SUPPORT LIBRARIES
# Install TensorFlow GPU version.
RUN pip3 --no-cache-dir install \
https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-1.11.0-cp36-cp36m-linux_x86_64.whl
# Install keras, theano, torch
RUN pip3 --no-cache-dir install \
    keras==2.2.3 \
    Theano==1.0.3 \
    torch==0.4.1

# Update environment variable accordingly
ENV LD_LIBRARY_PATH /usr/local/cuda/lib64:/usr/local/cuda-9.0/lib64:/usr/local/nvidia/lib:/usr/local/nvidia/lib64