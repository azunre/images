FROM nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04

ENV DEBIAN_FRONTEND noninteractive

# apt-utils seems missing and warnings are shown, so we install it.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-utils tzdata locales file sudo && \
 echo 'UTC' > /etc/timezone && \
 rm /etc/localtime && \
 dpkg-reconfigure tzdata && \
 apt-get upgrade --yes --force-yes && \
 rm -f /etc/cron.weekly/fstrim && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Prevent Python packages to be installed through APT.
COPY ./etc/apt/preferences.d /etc/apt/preferences.d

# General dependencies for building dependencies and locales.
RUN sed -i 's/^# deb-src/deb-src/' /etc/apt/sources.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends swig git build-essential cmake wget ssh tar gzip ca-certificates unzip curl libcurl4-openssl-dev libssl-dev && \
 locale-gen --no-purge en_US.UTF-8 && \
 update-locale LANG=en_US.UTF-8 && \
 echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | debconf-set-selections && \
 echo locales locales/default_environment_locale select en_US.UTF-8 | debconf-set-selections && \
 dpkg-reconfigure locales && \
 curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
 apt-get install --yes --force-yes git-lfs && \
 git lfs install && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Set locale to UTF-8 which makes Python read text in UTF-8 and not ASCII, but default.
ENV LC_ALL=en_US.UTF-8

# apt-get build-dep python3 without Python itself.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends autotools-dev bsdmainutils debhelper debiandoc-sgml \
 dh-strip-nondeterminism distro-info-data docutils-common file gettext gettext-base groff-base intltool-debian \
 libarchive-zip-perl libasprintf0v5 libcroco3 libfile-stripnondeterminism-perl libglib2.0-0 libhtml-parser-perl \
 libhtml-tagset-perl libmagic1 libmpdec2 libosp5 libpipeline1 libroman-perl libsgmls-perl libtext-format-perl \
 libtimedate-perl libunistring0 liburi-perl man-db mime-support opensp po-debconf sgml-data sgmlspl && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Extra Python dependencies.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends zlib1g-dev libexpat1-dev tk8.6-dev libffi-dev libssl-dev \
 libbz2-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libgdbm-dev liblzma-dev lzma lzma-dev && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Compiling Python 3.6.
RUN cd /usr/src && \
 wget --no-verbose https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz && \
 tar xzf Python-3.6.3.tgz && \
 cd Python-3.6.3 && \
 ./configure --enable-optimizations --enable-shared --enable-loadable-sqlite-extensions --with-system-expat --with-system-ffi --enable-ipv6 && \
 make -j8 install && \
 ldconfig && \
 cd / && rm -rf /usr/src/*

# Base Python packages (those dependencies from d3m core package which are needed also in testing image).
RUN pip3 install docker[tls]==2.7 && \
 pip3 install --upgrade setuptools && \
 pip3 install frozendict==1.2 && \
 pip3 install PyYAML==3.12 && \
 pip3 install pycurl==7.43.0.1 && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Matlab runtime for RPI primitives.
# TODO: Remove once a standard way to have custom dependencies is possible.
#       See: https://gitlab.com/datadrivendiscovery/metadata/issues/34
RUN cd /usr/src && \
 wget --no-verbose http://ssd.mathworks.com/supportfiles/downloads/R2017b/deployment_files/R2017b/installers/glnxa64/MCR_R2017b_glnxa64_installer.zip && \
 unzip MCR_R2017b_glnxa64_installer.zip && \
 mkdir -p /opt/mcr && ./install -mode silent -agreeToLicense yes -destinationFolder /opt/mcr && \
 echo /opt/mcr/v93/runtime/glnxa64 > /etc/ld.so.conf.d/zzz-matlab.conf && \
 echo /opt/mcr/v93/bin/glnxa64 >> /etc/ld.so.conf.d/zzz-matlab.conf && \
 echo /opt/mcr/v93/sys/os/glnxa64 >> /etc/ld.so.conf.d/zzz-matlab.conf && \
 ldconfig && \
 cd / && rm -rf /usr/src/*

# Compiling Boost for CMU primitives. We have to compile it ourselves against the custom Python and cannot use Debian package.
# Includes a symlink workaround for: https://svn.boost.org/trac10/ticket/11120
RUN cd /usr/src && \
 wget --no-verbose https://dl.bintray.com/boostorg/release/1.65.1/source/boost_1_65_1.tar.gz && \
 tar xzf boost_1_65_1.tar.gz && \
 cd boost_1_65_1 && \
 ln -s /usr/local/include/python3.6m /usr/local/include/python3.6 && \
 ./bootstrap.sh --with-python=$(which python3) && \
 ./b2 install && \
 rm /usr/local/include/python3.6 && \
 ldconfig && \
 cd / && rm -rf /usr/src/*

## ADDITIONAL GPU BASE IMAGE LIBRARIES
# Install TensorFlow GPU version.
RUN pip3 --no-cache-dir install \
https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow_gpu-1.11.0-cp36-cp36m-linux_x86_64.whl
# Install keras, theano, torch
RUN pip3 --no-cache-dir install \
    keras==2.2.3 \
    Theano==1.0.3 \
    torch==0.4.1
    #http://download.pytorch.org/whl/cu90/torch-0.4.1.post2-cp36-cp36m-linux_x86_64.whl

# As a final step, update some symbolic links
RUN ln -f -s /usr/local/cuda-9.0/targets/x86_64-linux/lib/stubs/libcuda.so /usr/lib/x86_64-linux-gnu/libcuda.so.1