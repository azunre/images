FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-xenial-python36

ENV GOPATH=/usr/local/go
ENV PATH="${GOPATH}/bin:${PATH}"

# Python.
RUN \
 pip3 install pycodestyle==2.3.1 && \
 pip3 install git+https://github.com/python/mypy.git@3edf07b92508d7387f8d372b0aca4ee025c0996e && \
 pip3 install deepdiff==3.3.0 && \
 pip3 install grpcio grpcio-tools && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Go.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes golang-go git curl unzip wget && \
 go get -u github.com/golang/protobuf/proto && \
 go get -u github.com/golang/protobuf/protoc-gen-go && \
 go get -u google.golang.org/grpc && \
 go get github.com/ckaznocha/protoc-gen-lint && \
 curl -OL https://github.com/google/protobuf/releases/download/v3.3.0/protoc-3.3.0-linux-x86_64.zip && \
 unzip protoc-3.3.0-linux-x86_64.zip -d protoc3 && \
 mv protoc3/bin/protoc /usr/bin/protoc && \
 rm -f protoc-3.3.0-linux-x86_64.zip && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# JavaScript.
RUN wget -O - https://nodejs.org/dist/v6.11.2/node-v6.11.2-linux-x64.tar.xz | tar Jx --strip=1 -C /usr/local --anchored --exclude=node-v6.11.2-linux-x64/CHANGELOG.md --exclude=node-v6.11.2-linux-x64/LICENSE --exclude=node-v6.11.2-linux-x64/README.md && \
 npm install grpc && \
 npm install google-protobuf && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
