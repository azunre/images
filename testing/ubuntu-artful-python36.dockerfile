FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-artful-python36

ENV GOPATH=/usr/local/go
ENV PATH="${GOPATH}/bin:${PATH}"
VOLUME /var/lib/docker

# Python.
RUN \
 apt-get update -q -q && \
 apt-get install --yes --force-yes build-essential libcap-dev ffmpeg && \
 pip3 install python-prctl==1.7 && \
 pip3 install pycodestyle==2.4.0 && \
 pip3 install git+https://github.com/python/mypy.git@3edf07b92508d7387f8d372b0aca4ee025c0996e && \
 pip3 install deepdiff==3.3.0 && \
 pip3 install pyquery==1.4.0 && \
 pip3 install yattag==1.10.0 && \
 pip3 install PyYAML==3.12 && \
 pip3 install grpcio grpcio-tools && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Go.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes golang-go git curl unzip wget && \
 go get -u github.com/golang/protobuf/proto && \
 go get -u github.com/golang/protobuf/protoc-gen-go && \
 go get -u google.golang.org/grpc && \
 go get github.com/ckaznocha/protoc-gen-lint && \
 curl -OL https://github.com/google/protobuf/releases/download/v3.6.1/protoc-3.6.1-linux-x86_64.zip && \
 unzip protoc-3.6.1-linux-x86_64.zip -d protoc3 && \
 cp -a protoc3/bin/protoc /usr/bin/protoc && \
 mkdir -p /usr/local/include/google && \
 cp -a protoc3/include/google/protobuf /usr/local/include/google && \
 rm -rf protoc-3.6.1-linux-x86_64.zip && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# JavaScript.
RUN wget -O - https://nodejs.org/dist/v6.11.2/node-v6.11.2-linux-x64.tar.xz | tar Jx --strip=1 -C /usr/local --anchored --exclude=node-v6.11.2-linux-x64/CHANGELOG.md --exclude=node-v6.11.2-linux-x64/LICENSE --exclude=node-v6.11.2-linux-x64/README.md && \
 npm install -g npm@^6.4.0 && \
 npm install grpc && \
 npm install google-protobuf && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Kubernetes in Docker.
RUN \
 go get github.com/mitar/kind && \
 curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
 echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes kubectl && \
 pip3 install git+https://github.com/mitar/python.git && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm
