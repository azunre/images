A repository with multiple Docker images used in the D3M program. Currently supported images are:

* `registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-artful-python36` — Base Docker image with Python 3.6.
* `registry.gitlab.com/datadrivendiscovery/images/testing:ubuntu-artful-python36` — Docker image with Python 3.6, Go, JavaScript/node.js used for CI testing in other repositories.
* `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-artful-python36-<version>` — Docker image with Python 3.6 and
  [core D3M Python package](https://gitlab.com/datadrivendiscovery/d3m) installed.
  `<version>` stands for a particular release of that package. There is also an `ubuntu-artful-python36-devel` Docker
  tag which corresponds to an image which is automatically build from the latest development code in `devel` branch
  of the package.

See [Docker image registry](https://gitlab.com/datadrivendiscovery/images/container_registry) for current list
of all images and their tags.

`core` images (except for `devel` image) are being build and tagged with a release version of the core package and if you
use one of those tagged images you can be assured that the core package will not change under you.

Despite this though, images still change through time because base images change with security and other similar updates
to system packages and dependencies. Those updates should never introduce any observable changes.
